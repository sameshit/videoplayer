#import <Cocoa/Cocoa.h>
#import "VPHelper.h"

@interface MyView : NSView
{
    VideoPlayerLib::VPHelper *video_player;
}

@property (readwrite,assign) VideoPlayerLib::VPHelper *video_player;

@end
