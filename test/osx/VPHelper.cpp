//
//  VPHelper.cpp
//  VideoPlayer
//
//  Created by Oleg on 16.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "VPHelper.h"

using namespace VideoPlayerLib;
using namespace CoreObjectLib;
using namespace std;
using namespace PeerLib;
using namespace MProtoLib;

const char*     kTrackerIP = "192.168.1.5";
const uint16_t  kTrackerPort = 12345;
const char*     kStun2IP   = "192.168.1.66";
const uint16_t  kStun1Port = 12466;
const uint16_t  kStun2Port = 12344;

VPHelper::VPHelper(CoreObject *core,CALayer *layer)
:VideoPlayer(core,layer)
{
    _run_tt.OnTimeTask.Attach(this, &VPHelper::ProcessRun);
    _run_tt.SetTimeout(0);
    
//    _peer_settings.request_logging = true;
    
    PeerSettings()->log_statistic = true;
    PeerSettings()->log_hybrid_control = true;
    
    OnPeerConnect.Attach(this,&VPHelper::ProcessNeighborConnect);
    OnPeerDisconnect.Attach(this,&VPHelper::ProcessNeighborDisconnect);
    OnTrackerDisconnect.Attach(this,&VPHelper::ProcessTrackerDisconnect);
    OnTrackerConnect.Attach(this,&VPHelper::ProcessTrackerConnect);
}

VPHelper::~VPHelper()
{
    
}

bool VPHelper::Run()
{
    if (!Bind(12888))
        return false;
    
    _core->GetScheduler()->Add(&_run_tt);
    
    return true;
}


void VPHelper::ProcessRun()
{
    LOG "connecting to tracker..."EL;
    
    if (!ConnectToTracker(kTrackerIP, kTrackerPort, kStun1Port, kStun2IP, kStun2Port))
    {
        LOG "Couldn't connect to tracker" EL;
        return;
    }
    if (!PlayStream("simpsons_hd"))
    {
        LOG "Coudln't play stream"EL;
        return;
    }



/*    if (!PlayFile("/Users/void/easy.tv/ffmpeg-latest/test.live"))
        return;*/
}

void VPHelper::ProcessNeighborConnect(MProtoLib::MSession *session)
{
    LOG "Neighbor "<<PRINT_SESSION_ADDR(session)<<" connected"EL;
}

void VPHelper::ProcessNeighborDisconnect(MProtoLib::MSession *session)
{
    LOG "Neighbor "<<PRINT_SESSION_ADDR(session)<<" disconnected with reason: "<<session->GetDisconnectReason().str() EL;
}

void VPHelper::ProcessTrackerConnect(MProtoLib::MSession *session)
{
    LOG "Connected to tracker "<<PRINT_SESSION_ADDR(session) EL;
}

void VPHelper::ProcessTrackerDisconnect(MProtoLib::MSession *session)
{
    LOG "tracker connection lost, trying to reconnect after 2 seconds with reason: "<<session->GetDisconnectReason().str() EL;
    _run_tt.SetTimeout(2000);
    _core->GetScheduler()->Add(&_run_tt);
}

void VPHelper::ProcessReadError()
{
    LOG "LiveReader error: "<<COErr::Get() EL;
}