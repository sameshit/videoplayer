//
//  AppDelegate.m
//  GeneralTest
//
//  Created by Oleg Gordiychuck on 01.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
using namespace VideoPlayerLib;
using namespace CoreObjectLib;
using namespace DecoderLib;

@implementation AppDelegate

@synthesize window = window;


- (void)dealloc
{

    [super dealloc];
}



- (void)applicationWillTerminate:(NSNotification *)notification
{
    
    delete _video_player;
    [_my_view release];
    delete  _core;
    
}



-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSLog(@"app finished");
    _core = new CoreObject;
    
    _my_view = [[MyView alloc] init];
    
    [_my_view setWantsLayer:YES];
    [window setContentView:_my_view];
    
    
    _video_player = new VPHelper(_core,[[window contentView] layer]);
    [_my_view setVideo_player:_video_player];
    
    if (!_video_player->Run())
        LOG "run error" EL;
}

@end
