//
//  AppDelegate.h
//  GeneralTest
//
//  Created by Oleg Gordiychuck on 01.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "VPHelper.h"
#import "MyView.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>
{
    NSWindow *window;
    @private
        VideoPlayerLib::VPHelper    *_video_player;
        CoreObjectLib::CoreObject   *_core;
        VideoPlayerLib::AudioDevice *_audio_device;
        MyView                      *_my_view;
}
@property (assign) IBOutlet NSWindow *window;

@end
