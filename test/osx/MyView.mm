#import "MyView.h"

using namespace VideoPlayerLib;

@implementation MyView

@synthesize video_player = video_player;

//static uint16_t volume = UINT16_MAX/2;

- (void) mouseDown:(NSEvent *)theEvent
{
/*    _video_player->SetVolume(volume);
    
    if (volume == UINT16_MAX/2)
        volume = UINT16_MAX;
    else 
        volume = UINT16_MAX/2;*/
    video_player->ToggleFullScreen();
}

@end