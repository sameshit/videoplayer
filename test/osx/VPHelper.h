//
//  VPHelper.h
//  VideoPlayer
//
//  Created by Oleg on 16.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __VideoPlayer__VPHelper__
#define __VideoPlayer__VPHelper__

#include "../../src/VideoPlayer.h"

namespace VideoPlayerLib
{
    class VPHelper
    :public VideoPlayer
    {
    private:
        CoreObjectLib::TimeTask _run_tt;
        
        void ProcessReadError();
        void ProcessRun();
        void ProcessTrackerConnect(MProtoLib::MSession *session);
        void ProcessTrackerDisconnect(MProtoLib::MSession *session);
        void ProcessNeighborConnect(MProtoLib::MSession *session);
        void ProcessNeighborDisconnect(MProtoLib::MSession *session);
    public:
        VPHelper(CoreObjectLib::CoreObject *core,CALayer *layer);
        virtual ~VPHelper();
        
        bool Run();
    };
}

#endif /* defined(__VideoPlayer__VPHelper__) */
