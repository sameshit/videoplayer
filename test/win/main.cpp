#include "../../src/VideoPlayer.h"

using namespace CoreObjectLib;
using namespace VideoPlayerLib;
using namespace std;

VideoPlayer *video_player;
CoreObject *_core;

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{

	switch(uMsg)
	{
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:
		{
			switch(LOWORD(wParam))
			{
			case VK_ESCAPE:
				DestroyWindow(hWnd);
				break;
			case VK_F1:
				video_player->ToggleFullScreen();
				break;
			default:

				break;
			}
			
			break;
		}
	case WM_LBUTTONDOWN:
		{

			break;
		}
	case WM_RBUTTONDOWN:
		{

			break;
		}
	case WM_USER:
		{

		}
	default:
		break;
	}
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}


INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR, INT)
{
	WNDCLASS wndClass = { sizeof(WNDCLASS) };

	AllocConsole();
	freopen("CONIN$", "r", stdin);
	freopen("CONOUT$", "w", stdout);
	freopen("CONOUT$", "w", stderr);
	
	_core = new CoreObject();

	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hbrBackground = (HBRUSH)(COLOR_WINDOW);
	wndClass.hCursor = LoadCursor(0, IDC_ARROW);
	wndClass.hIcon = LoadIcon(0, IDI_APPLICATION);
	wndClass.hInstance = hInst;
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = TEXT("ShaderWindow");
	wndClass.lpszMenuName = 0;
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	if(!RegisterClass(&wndClass))
	{
		MessageBox(0, TEXT("Cannot register window class!"), TEXT("Error!"), MB_OK | MB_ICONERROR);
		return -1;
	}

	HWND hWnd = CreateWindow(TEXT("ShaderWindow"), TEXT("Shaders"), WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME ^ WS_MAXIMIZEBOX^WS_SIZEBOX , 
		CW_USEDEFAULT, 0, 800, 600, 0, 0, 0, 0);

	if(!hWnd)
	{
		MessageBox(0, TEXT("Cannot create window!"), TEXT("Error!"), MB_OK | MB_ICONERROR);
		return -2;
	}

	ShowWindow(hWnd, SW_NORMAL);
	UpdateWindow(hWnd);
	LOG_INFO("IT WORKS");
	fast_new(video_player,_core);
	video_player->SetDrawingSurface(hWnd);

	ASSERT_ERROR(video_player->PlayFile("C:/opusvp8.live"));
		
	MSG msg;

	while(GetMessage(&msg, 0, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}


	fast_delete(video_player);

	delete _core;


	return 0;
}