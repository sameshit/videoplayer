//
//  main.cpp
//  VideoPlayer
//
//  Created by Oleg on 12.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "../../src/VideoPlayer.h"

using namespace CoreObjectLib;
using namespace VideoPlayerLib;
using namespace DecoderLib;

int main()
{
    CoreObjectLib::CoreObject *_core;
    AudioDevice *audio_device;
    Frame frame;
    uint8_t *pos;
    AudioHeader header;
    
    _core = new CoreObject;
    fast_new1(AudioDevice,audio_device,_core);
    
    header.channels = 2;
    header.sample_rate = 48000;
    
    if (!audio_device->Open(header))
    {
        LOG "can't open audio device" EL;
        return - 1;
    }
    
    frame.data.resize(4*1024);
    pos = (uint8_t*)frame.data.c_str();
    while (true)
    {
        for (int i = 0 ; i < frame.data.size(); ++i)
            pos[i] = rand() % 255;
        
        if (!audio_device->Play(frame))
        {
            LOG "play failed" EL;
            break;
        }
    }
    
    fast_delete(AudioDevice,audio_device);
    delete _core;
    return 0;
}
