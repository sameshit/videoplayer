//
//  VideoPlayer.h
//  
//
//  Created by Oleg on 12.02.13.
//
//

#ifndef ____VideoPlayer__
#define ____VideoPlayer__

#include "audiodevice/AudioDevice.h"
#include "videodevice/VideoDevice.h"

namespace VideoPlayerLib
{
    class LIBEXPORT VideoPlayer
    :public DecoderLib::Decoder
    {
	public:
        VideoPlayer(CoreObjectLib::CoreObject *core);
        virtual ~VideoPlayer();

#if defined(OS_X)
        bool SetDrawingSurface(CALayer *surface);
#elif defined(OS_WIN)
		bool SetDrawingSurface(HWND surface);
#endif
        
        void ToggleFullScreen();
    private:
        uint32_t _audio_pts,_prev_audio_pts,_video_pts,_prev_video_pts;
		uint32_t _audio_delay;
        uint64_t _audio_time,_video_time;
        CoreObjectLib::SpinLock _sync_lock;
        AudioDevice *_audio_device;
        VideoDevice *_video_device;
        CoreObjectLib::Thread *_video_thread,*_audio_thread,*_read_thread;
        CoreObjectLib::VideoHeader _video_header;
        CoreObjectLib::AudioHeader _audio_header;
		DecoderLib::PixelFormat _pixel_format;
        bool _need_audio_delay,_need_video_delay;
        CoreObjectLib::Semaphore _video_sem;
        
        bool PlayAudioFrame(const CoreObjectLib::Frame& frame);
        bool PlayVideoFrame(const CoreObjectLib::Frame& frame);
        void ProcessVideoFrame();
        void ProcessAudioFrame();
        void ProcessAudioDecoderOpened(const CoreObjectLib::AudioHeader &header);
        void ProcessVideoDecoderOpened(const CoreObjectLib::VideoHeader &header,
                                       const DecoderLib::PixelFormat &pf);
        void ProcessAudioDecoderClosed();
        void ProcessVideoDecoderClosed();
#if defined(OS_X)
        CALayer *_surface;
#elif defined(OS_WIN)
		HWND _surface;
#endif
    };
}

#endif /* defined(____VideoPlayer__) */
