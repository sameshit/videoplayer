//
//  VideoPlayer.cpp
//  
//
//  Created by Oleg on 12.02.13.
//
//

#include "VideoPlayer.h"

using namespace CoreObjectLib;
using namespace DecoderLib;
using namespace VideoPlayerLib;
using namespace std;
using namespace PeerLib;


VideoPlayer::VideoPlayer(CoreObject *core)
:Decoder(core),_surface(nullptr)
{
    _video_thread = _core->GetThread();
    _audio_thread = _core->GetThread();
    
    _audio_thread->OnThread.Attach(this,&VideoPlayer::ProcessAudioFrame);
	_video_thread->OnThread.Attach(this,&VideoPlayer::ProcessVideoFrame);
    
    fast_new(_audio_device,_core);
    fast_new(_video_device,_core);
    
    OnAudioDecoderOpened.Attach(this, &VideoPlayer::ProcessAudioDecoderOpened);
    OnVideoDecoderOpened.Attach(this, &VideoPlayer::ProcessVideoDecoderOpened);
    OnAudioDecoderClosed.Attach(this,&VideoPlayer::ProcessAudioDecoderClosed);
    OnVideoDecoderClosed.Attach(this,&VideoPlayer::ProcessVideoDecoderClosed);
}

VideoPlayer::~VideoPlayer()
{
    PeerDeleteFromLoop();
    GetAudioDecoder()->Close();
    GetVideoDecoder()->Close();
    
    _core->ReleaseThread(&_video_thread);
    _core->ReleaseThread(&_audio_thread);
    fast_delete(_audio_device);
    fast_delete(_video_device);
}

#if defined(OS_X)
bool VideoPlayer::SetDrawingSurface(CALayer *surface)
#elif defined(OS_WIN)
bool VideoPlayer::SetDrawingSurface(HWND surface)
#endif
{
	RETURN_MSG_IF_TRUE(_surface != nullptr,"Surface is already set");
	_surface = surface;
	return true;
}

void VideoPlayer::ProcessAudioDecoderOpened(const AudioHeader &header)
{
    _audio_header = header;
    if (!_audio_device->Open(_audio_header))
    {
        LOG_ERROR( "Couldn't open audio device. Error: "<<COErr::Get());
        return;
    }
    COTime now;
    _audio_time = now();
    _audio_pts = 0;
    _need_audio_delay = false;
	_audio_delay = 0;
    _audio_thread->Wake();
}

void VideoPlayer::ProcessAudioDecoderClosed()
{
    _audio_thread->Sleep();
    if (!_audio_device->Close())
    {
        LOG_ERROR( "Couldn't close audio device. Error: "<<COErr::Get() );
        return;
    }
}

uint32_t prev_pts = 0;
uint64_t prev_time = 0;

void VideoPlayer::ProcessAudioFrame()
{
    Frame frame;
	uint32_t audio_delay;
	bool need_audio_delay;
    
    if (!GetAudioDecoder()->PopFrame(&frame))
        return;
		
    COTime now;
    _sync_lock.Lock();
	need_audio_delay = _need_audio_delay;
	audio_delay = _audio_delay;
    _audio_time = now();
    _audio_pts = frame.pts;
    _sync_lock.UnLock();

	if (need_audio_delay)
	{
		LOG_INFO("Audio is far ahead video and delayed for "<<audio_delay<<" milliseconds");
		Utils::Delay(audio_delay);
		_sync_lock.Lock();
		_need_audio_delay = false;
		_sync_lock.UnLock();
	}
    if (!_audio_device->Play(&frame))
    {
        LOG_ERROR("Couldn't play audio frame. Error: "<<COErr::Get());
        fast_free(frame.data);
        return;
    }    
}

void VideoPlayer::ProcessVideoDecoderOpened(const CoreObjectLib::VideoHeader &header,const PixelFormat &pf)
{
	_video_header = header;
	if (_surface == nullptr)
	{
		LOG_ERROR( "Video device wasn't opened due to empty surface. Set drawing surface before start of decoding" );
		return;
	}

	if (!_video_device->Open(_video_header,pf,_surface))
    {
        LOG_ERROR( "Couldn't open video device. Error: "<<COErr::Get() );
        return;
    }
	_need_video_delay = true;
    _video_thread->Wake();
}

void VideoPlayer::ProcessVideoDecoderClosed()
{
    _video_thread->Sleep();
    if (!_video_device->Close())
    {
		LOG_ERROR("Couldn't close audio device. Error: "<<COErr::Get());
        return;
    }
}

void VideoPlayer::ProcessVideoFrame()
{
    uint32_t audio_pts;
    uint64_t audio_time;
    Frame frame;
    
    if (!GetVideoDecoder()->PopFrame(&frame))
        return;
    
    _sync_lock.Lock();
    audio_pts = _audio_pts;
    audio_time = _audio_time;
    _sync_lock.UnLock();
    
    COTime now;
    audio_pts += now()-audio_time;
    if (audio_pts < frame.pts)
    {
        if (frame.pts-audio_pts < 3000)
            Utils::Delay(frame.pts-audio_pts);
    }
    else if (audio_pts - frame.pts >200)
	{
		_sync_lock.Lock();
		if (!_need_audio_delay)
		{
			_audio_delay = audio_pts - frame.pts;  
			_need_audio_delay = true;
		}
		_sync_lock.UnLock();
        Utils::Delay(1000/24);
	}
    if(!_video_device->Draw(&frame))
		LOG_ERROR("VideoDevice::Draw failed with error: "<<COErr::Get());
}

void VideoPlayer::ToggleFullScreen()
{
    _video_device->ToggleFullScreen();
}