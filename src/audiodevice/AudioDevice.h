#ifndef AUDIODEVICE_H_
#define AUDIODEVICE_H_

#include "../../Decoder/src/Decoder.h"

#define AUDIO_DEVICE_BUFFER_COUNT 2*10

#if defined(OS_X)
#import <AudioToolbox/AudioToolbox.h>
#import <CoreAudio/CoreAudio.h>
#elif defined(OS_WIN)
#define DIRECTSOUND_VERSION 0x1000
#include <DSound.h>
#endif

namespace VideoPlayerLib
{

class AudioDevice
{
public:
	AudioDevice(CoreObjectLib::CoreObject *core);
	virtual ~AudioDevice();

	bool Open(const CoreObjectLib::AudioHeader &header);
	bool Play(const CoreObjectLib::Frame *frame);
	bool Close();

    bool GetVolume(uint16_t *volume);
    bool SetVolume(uint16_t volume);
private:
	CoreObjectLib::CoreObject               *_core;
    CoreObjectLib::AudioHeader              _audio_settings;
   	bool                                    _opened;
    CoreObjectLib::Semaphore                *_sem;
#ifdef OS_LINUX
    snd_pcm_t                               *_handle;
#elif defined(OS_WIN)
	WAVEFORMATEX                            _format;
	LPDIRECTSOUND8							_direct_sound;
	LPDIRECTSOUNDBUFFER8					_sound_buffer;
	LPDIRECTSOUNDBUFFER						_primary_buffer;
	LPDIRECTSOUNDNOTIFY8					_notify;
	HANDLE									_notify_events[3];
	DSBPOSITIONNOTIFY						_notify_pos[2];
	DSBUFFERDESC							_buffer_format;
	uint8_t									*_unused_data;
	size_t									_unused_data_size,_unused_data_pos;
	std::queue<CoreObjectLib::Frame>		_frame_q;
	CoreObjectLib::SpinLock					_write_lock,_close_lock;
	CoreObjectLib::Thread					*_audio_thread;
	bool									_is_close;

	void AudioQueueThread();
#elif defined(OS_X)
    AudioQueueRef                           _aq;
    AudioQueueBufferRef                     _buffers[AUDIO_DEVICE_BUFFER_COUNT];
    static void AudioCallback               (void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer);
#endif
};

}
#endif /* AUDIODEVICE_H_ */
