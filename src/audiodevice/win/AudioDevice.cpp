#include "../AudioDevice.h"

using namespace VideoPlayerLib;
using namespace CoreObjectLib;
using namespace DecoderLib;
using namespace std;

AudioDevice::AudioDevice(CoreObject *core)
	:_core(core),_opened(false)
{
	_notify_events[0] = CreateEvent(NULL,false,false,NULL);
	_notify_events[1] = CreateEvent(NULL,false,false,NULL);
	_notify_events[2] = CreateEvent(NULL,false,false,NULL);

	_notify_pos[0].hEventNotify = _notify_events[0];
	_notify_pos[1].hEventNotify = _notify_events[1];
	_audio_thread = _core->GetThread();
	_audio_thread->OnThread.Attach(this,&AudioDevice::AudioQueueThread);

	fast_new(_sem,AUDIO_DEVICE_BUFFER_COUNT);
}

AudioDevice::~AudioDevice()
{
	uint8_t *data;
    if(_opened)
        Close();

	_core->ReleaseThread(&_audio_thread);
	CloseHandle(_notify_events[0]);
	CloseHandle(_notify_events[1]);
	CloseHandle(_notify_events[2]);

	fast_delete(_sem);
	while (!_frame_q.empty())
	{
		data = _frame_q.front().data;
		fast_free(data);
		_frame_q.pop();
	}
}

bool AudioDevice::Open(const AudioHeader &header)
{
		ComUniquePtr<IDirectSound8> udirect_sound;
		HRESULT hr;
		ComUniquePtr<IDirectSoundBuffer> usound_buffer,uprimary_buffer;
		ComUniquePtr<IDirectSoundNotify8> unotify;
		ComUniquePtr<IDirectSoundBuffer8> usound_buffer8;
		
		RETURN_MSG_IF_TRUE(_opened,"AudioDevice is already opened");

		RETURN_MSG_IF_TRUE(FAILED((DirectSoundCreate8 (NULL,udirect_sound.Receive(), NULL))),"Couldn't create direct sound.");
		RETURN_MSG_IF_TRUE(FAILED(udirect_sound->SetCooperativeLevel(GetDesktopWindow(),DSSCL_PRIORITY)),"Couldn't set cooperative level");

		memset(&_buffer_format,0,sizeof(_buffer_format));
		_buffer_format.dwSize = sizeof(DSBUFFERDESC);
		_buffer_format.dwFlags = DSBCAPS_PRIMARYBUFFER;

		RETURN_MSG_IF_TRUE(FAILED(hr = udirect_sound->CreateSoundBuffer(&_buffer_format,uprimary_buffer.Receive(),NULL)),
			"Couldn't create primary sound buffer. Error: "<<WinDecoderUtils::HResultToString(hr));

        _audio_settings = header;

        memset(&_format,0,sizeof(WAVEFORMATEX) );
        _format.wFormatTag = WAVE_FORMAT_PCM;
        _format.nChannels  = _audio_settings.channels;
        _format.wBitsPerSample = 16;
		_format.nSamplesPerSec = _audio_settings.sample_rate;
        _format.nBlockAlign = _format.nChannels * _format.wBitsPerSample / 8;
        _format.nAvgBytesPerSec = _format.nBlockAlign * _format.nSamplesPerSec ;        
        _format.cbSize = 0;

		RETURN_MSG_IF_TRUE(FAILED(hr = uprimary_buffer->SetFormat(&_format)),
			"Couldn't set format. Error: "<<WinDecoderUtils::HResultToString(hr));

		_buffer_format.dwSize = sizeof(DSBUFFERDESC);
		_buffer_format.dwFlags = DSBCAPS_CTRLVOLUME | DSBCAPS_TRUEPLAYPOSITION
			| DSBCAPS_GLOBALFOCUS |DSBCAPS_CTRLPOSITIONNOTIFY | DSBCAPS_GETCURRENTPOSITION2;
		_buffer_format.dwBufferBytes = 60*_format.nAvgBytesPerSec/1000;
		_buffer_format.dwReserved = 0;
		_buffer_format.lpwfxFormat =&_format;
		_buffer_format.guid3DAlgorithm = GUID_NULL;

		RETURN_MSG_IF_TRUE(FAILED(hr = udirect_sound->CreateSoundBuffer(&_buffer_format,usound_buffer.Receive(),NULL)),
			"Couldn't create sound buffer. Error: "<<WinDecoderUtils::HResultToString(hr));
		RETURN_MSG_IF_TRUE(FAILED(usound_buffer->QueryInterface(IID_IDirectSoundBuffer8,(LPVOID*)usound_buffer8.Receive())),
			"Couldn't query sound buffer");

		RETURN_MSG_IF_TRUE(FAILED(hr = usound_buffer8->SetCurrentPosition(0)),"Couldn't set current position");

		RETURN_MSG_IF_TRUE(FAILED(usound_buffer8->QueryInterface(IID_IDirectSoundNotify8,(LPVOID*)unotify.Receive())),
			"Couldn't query notify");

		RETURN_MSG_IF_FALSE(ResetEvent(_notify_events[0])==TRUE,"ResetEvent 0 failed");
		RETURN_MSG_IF_FALSE(ResetEvent(_notify_events[1])==TRUE,"ResetEvent 1 failed");
		RETURN_MSG_IF_FALSE(ResetEvent(_notify_events[2])==TRUE,"ResetEvent 2 failed");

		_notify_pos[0].dwOffset = 1000;
		_notify_pos[1].dwOffset = _buffer_format.dwBufferBytes/2+1000;
		_unused_data_size = 0;
		_unused_data_pos = 0;
		_unused_data = nullptr;
		RETURN_MSG_IF_TRUE(FAILED(hr = unotify->SetNotificationPositions(2,_notify_pos)),
			"Couldn't set notification positions. Error: "<<WinDecoderUtils::HResultToString(hr));

		_is_close=false;

		RETURN_IF_FALSE(_audio_thread->Wake());

		RETURN_MSG_IF_TRUE(FAILED(usound_buffer8->Play(0,0,DSBPLAY_LOOPING)),
			"Couldn't play buffer. Error: "<<WinDecoderUtils::HResultToString(hr));

		_direct_sound = udirect_sound.Detach();
		_sound_buffer = usound_buffer8.Detach();
		_notify = unotify.Detach();
		_primary_buffer = uprimary_buffer.Detach();
        _opened = true;
        return true;
}

bool AudioDevice::Close()
{
        RETURN_MSG_IF_FALSE(_opened, "Audio device is already opened");
		
		_close_lock.Lock();
		_is_close = true;
		_close_lock.UnLock();
		
		SetEvent(_notify_events[2]);
		_audio_thread->Sleep();
		_sound_buffer->Release();
		_notify->Release();
		_primary_buffer->Release();
		_direct_sound->Release();
		if (_unused_data != nullptr)
			fast_free(_unused_data);

		return true;
}

bool AudioDevice::Play(const Frame *frame)
{	
	RETURN_IF_FALSE(_sem->Wait());

	_write_lock.Lock();
	_frame_q.push(*frame);
	_write_lock.UnLock();

	return true;
}


void AudioDevice::AudioQueueThread()
{
	DWORD wait_status,offset,size1,size2,rem_size,lock_size;
	uint8_t *pos1,*pos2,*cur_pos;
	bool is_close,empty_q;
	HRESULT hr;
	size_t copy_size;

	_close_lock.Lock();
	is_close = _is_close;
	_close_lock.UnLock();
	
	if (is_close)
		return;

	wait_status = WaitForMultipleObjects(3,_notify_events,false,INFINITE);
	switch(wait_status)
	{
	case WAIT_OBJECT_0:
		offset = _buffer_format.dwBufferBytes/2;
	break;
	case WAIT_OBJECT_0+1:
		offset = 0;
	break;
	case WAIT_OBJECT_0+2:
		return;
	break;
	default:
		FATAL_ERROR("AudioDevice::AudioQueueThread invalid wait_status("<<wait_status<<")");
	}
	lock_size =  _buffer_format.dwBufferBytes/2;
	rem_size = lock_size;
	hr = _sound_buffer->Lock(offset,lock_size,(LPVOID*)&pos1,&size1,(LPVOID*)&pos2,&size2,0);
	if (FAILED(hr))
		FATAL_ERROR("AudioDevice::AudioQueueThread Lock buffer failed. Error: "<<WinDecoderUtils::HResultToString(hr));
	assert(size1 == lock_size && size2==0);
	while (rem_size > 0)
	{
		cur_pos = pos1+(lock_size-rem_size);
		if (_unused_data != nullptr)
		{
			copy_size = min(_unused_data_size - _unused_data_pos,rem_size);
			memcpy(cur_pos,_unused_data+_unused_data_pos,copy_size);
			_unused_data_pos += copy_size;
			rem_size-=copy_size;
			assert(_unused_data_pos <= _unused_data_size);
			if (_unused_data_pos == _unused_data_size)
				fast_free(_unused_data);
		}
		else
		{
			_write_lock.Lock();
			empty_q = _frame_q.empty();
			if (!empty_q)
			{
				_unused_data = _frame_q.front().data;
				_unused_data_size = _frame_q.front().size;
				_unused_data_pos = 0;
				_frame_q.pop();
				_sem->Post();
			}
			_write_lock.UnLock();
			if (empty_q)
			{
				memset(cur_pos,0,rem_size);
				rem_size -= rem_size;
			}
		}
	}
	hr = _sound_buffer->Unlock(pos1,size1,pos2,size2);

	assert(SUCCEEDED(hr));

}