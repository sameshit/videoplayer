#include "YUV420Renderer.h"
#include "ShaderSource.h"

using namespace CoreObjectLib;
using namespace VideoPlayerLib;
using namespace std;
using namespace DecoderLib;
using namespace PeerLib;

YUV420Renderer::YUV420Renderer(CoreObject *core)
:BaseRenderer(core),_opened(false)
{

}

YUV420Renderer::~YUV420Renderer()
{
    
}

bool YUV420Renderer::Resize(const ImageSize &window_size,void *context_object)
{
	uint32_t new_height,new_width;
	float ratio;
	GLint pos_location;
	GLenum err;

    if (!_opened)
    {
        COErr::Set("YUV420Renderer is not in opened state in Resize()");
        return false;
    }
    
	_window_size = window_size;

	glViewport(0, 0, _window_size.width,_window_size.height);

	if ((err = glGetError()) != GL_NO_ERROR)
    {
        COErr::Set("Couldn't set viewport",err);
        return false;
    }


    new_width	 = _window_size.width;
    new_height   = _window_size.width*_header.height/_header.width;

    ratio = (float)new_height/(float)_window_size.height;
    if (ratio < 1.f)
        ratio = 1/ratio;

	_vertices[0] = -1.f; 	_vertices[1] = 1.f/ratio;		// left top
	_vertices[2] = -1.f;	_vertices[3] = -1.f/ratio;		// left bot
	_vertices[4] = 1.f;		_vertices[5] = 1.f/ratio;		// right top
	_vertices[6] = 1.f;		_vertices[7] = -1.f/ratio;		// right bot

	pos_location = glGetAttribLocation(_program, "in_pos");
	glEnableVertexAttribArray(pos_location);
	glVertexAttribPointer(pos_location, 2, GL_FLOAT, GL_FALSE, 0, _vertices);

	if ((err = glGetError()) != GL_NO_ERROR)
    {
        COErr::Set("Couldn't load vertecies",err);
        return false;
    }

	return true;
}

bool YUV420Renderer::Open(const VideoHeader &header,void *context_object)
{
	const char* vs_source = kVertexShader;
	ssize_t vs_size = strlen(kVertexShader);
	const char* ps_source = kFragmentShader;
	ssize_t ps_size = strlen(kFragmentShader);
	GLint result = GL_TRUE;
	char c_log[kErrorSize];
    int  len;
	string log;
	GLint yuv2rgb_location;
	GLint tc_location;
	GLenum err;
    
    if (_opened)
    {
        COErr::Set("YUV420Renderer is already in opened state");
        return false;
    }
    
    _header = header;

	glGenTextures(3, _textures);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _textures[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glEnable(GL_TEXTURE_2D);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, _textures[1]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glEnable(GL_TEXTURE_2D);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, _textures[2]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glEnable(GL_TEXTURE_2D);

	if ((err = glGetError()) != GL_NO_ERROR)
    {
        COErr::Set("Couldn't init textures",err);
		return false;
    }

	_program = glCreateProgram();
        assert(glGetError() == GL_NO_ERROR);

	_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(_vertex_shader, 1, &vs_source, (const GLint*)&vs_size);
	glCompileShader(_vertex_shader);
	glGetShaderiv(_vertex_shader, GL_COMPILE_STATUS, &result);

	if (!result)
	{
		glGetShaderInfoLog(_vertex_shader, kErrorSize - 1, &len, c_log);
		c_log[len + 1] = 0;
		log.assign(c_log,len+1);
        COErr::Set(log.c_str());
		return false;
	}

	glAttachShader(_program, _vertex_shader);
	glDeleteShader(_vertex_shader);

	_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(_fragment_shader, 1, &ps_source, (const GLint*)&ps_size);
	glCompileShader(_fragment_shader);

	glGetShaderiv(_fragment_shader, GL_COMPILE_STATUS, &result);

	if (!result)
	{
		glGetShaderInfoLog(_fragment_shader, kErrorSize - 1, &len, c_log);
		c_log[len+1] = 0;
		log.assign(c_log,len+1);
        COErr::Set(log.c_str());
        return false;
	}
	glAttachShader(_program, _fragment_shader);
	glDeleteShader(_fragment_shader);

	glLinkProgram(_program);
	glGetProgramiv(_program, GL_LINK_STATUS, &result);

	if (!result)
	{
		glGetProgramInfoLog(_program, kErrorSize - 1, &len, c_log);
		c_log[len - 1] = 0;
		log.assign(c_log,len+1);
        COErr::Set(log.c_str());
		return false;
	}

	glUseProgram(_program);
	glDeleteProgram(_program);

	glUniform1i(glGetUniformLocation(_program, "y_tex"), 0);
	glUniform1i(glGetUniformLocation(_program, "u_tex"), 1);
	glUniform1i(glGetUniformLocation(_program, "v_tex"), 2);
	yuv2rgb_location = glGetUniformLocation(_program, "yuv2rgb");

	glUniformMatrix3fv(yuv2rgb_location, 1, GL_TRUE, kYUV2RGB);

	tc_location = glGetAttribLocation(_program, "in_tc");
	glEnableVertexAttribArray(tc_location);
	glVertexAttribPointer(tc_location, 2, GL_FLOAT, GL_FALSE, 0,kTextureCoords);

	if ((err =glGetError()) != GL_NO_ERROR)
    {
        COErr::Set("Couldn't init opengl",err);
        return false;
    }

    glClearColor(0.f, 0.f, 0.f, 1.f);

    _opened = true;
	return true;
}

bool YUV420Renderer::Render(const Frame &frame,void *context_object)
{
    uint8_t *pos;
    GLenum err;
    
    if (!_opened)
    {
        COErr::Set("YUV420Renderer is not in opened state in Render()");
        return false;
    }
    
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//#ifdef OS_X
//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//#endif
    pos = (uint8_t*)frame.data;
    glActiveTexture(GL_TEXTURE0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, _header.width, _header.height, 0,
		GL_LUMINANCE, GL_UNSIGNED_BYTE,pos);

    pos += _header.width*_header.height;
	glActiveTexture(GL_TEXTURE1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, _header.width / 2, _header.height /2, 0,
		GL_LUMINANCE, GL_UNSIGNED_BYTE,pos);

    pos += _header.width*_header.height/4;
	glActiveTexture(GL_TEXTURE2);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, _header.width / 2, _header.height /2, 0,
		GL_LUMINANCE, GL_UNSIGNED_BYTE,pos);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
#ifdef OS_WIN
	glFlush();
#endif
    
    if ((err =glGetError()) != GL_NO_ERROR)
    {
        COErr::Set("YUV420Renderer failed to Render() with OpenGL error",err);
        return false;
    }
    
    return true;
}

bool YUV420Renderer::Clear(void *context_object)
{
    if (!_opened)
    {
        COErr::Set("YUV420Renderer is not in opened state in Clear()");
        return false;
    }
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glFlush();
    
    return true;
}

bool YUV420Renderer::Close(void *context_object)
{
    if (!_opened)
    {
        COErr::Set("YUV420Renderer is not in opened state in Close()");
        return false;
    }
    
    glDeleteTextures(3, _textures);
    
    _opened = false;
    return true;
}

void YUV420Renderer::FreeFrame(Frame *frame)
{
    fast_free(frame->data);
}