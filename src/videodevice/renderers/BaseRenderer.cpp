//
//  BaseRenderer.cpp
//  VideoPlayer
//
//  Created by Oleg on 04.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BaseRenderer.h"

using namespace VideoPlayerLib;
using namespace CoreObjectLib;
using namespace PeerLib;

BaseRenderer::BaseRenderer(CoreObject *core)
:_core(core)
{
    
}

BaseRenderer::~BaseRenderer()
{

}

bool BaseRenderer::Open(const VideoHeader &header,void *context_object)
{
    COErr::Set("INTERNAL ERROR: BaseRenderer::Open() called");
    return false;
}

bool BaseRenderer::Close(void *context_object)
{
    COErr::Set("INTERNAL ERROR: BaseRenderer::Close() called");
    return false;
}

bool BaseRenderer::Render(const Frame &frame,void *context_object)
{
    COErr::Set("INTERNAL ERROR: BaseRenderer::Render() called");
    return false;
}

bool BaseRenderer::Resize(const ImageSize &window_size,void *context_object)
{
    COErr::Set("INTERNAL ERROR: BaseRenderer::Resize() called");
    return false;
}

bool BaseRenderer::Clear(void *context_object)
{
    COErr::Set("INTERNAL ERROR: BaseRenderer::Clear() called");
    return false;
}

void BaseRenderer::FreeFrame(Frame *frame)
{
    FATAL_ERROR("INTERNAL ERROR: BaseRenderer::FreeFrame() called");
}