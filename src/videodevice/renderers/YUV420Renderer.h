#ifndef YUV420Renderer__H
#define YUV420Renderer__H

#include "BaseRenderer.h"

namespace VideoPlayerLib
{
	class YUV420Renderer
    :public BaseRenderer
	{
	private:
		ImageSize                       _window_size;
		GLuint                          _textures[3];
		GLuint                          _program,_vertex_shader;
		GLuint                          _fragment_shader;
		float                           _vertices[8];
        CoreObjectLib::VideoHeader      _header;
        bool                            _opened;
	public:
		YUV420Renderer(CoreObjectLib::CoreObject *core);
		virtual ~YUV420Renderer();

        bool Open       (const CoreObjectLib::VideoHeader &header,void *context_object=NULL);
        bool Close      (void *context_object=NULL);
        bool Clear      (void *context_object=NULL);
        bool Render     (const CoreObjectLib::Frame &frame,void *context_object=NULL);
        bool Resize     (const ImageSize &window_size,void *context_object=NULL);
        void FreeFrame  (CoreObjectLib::Frame *frame);
	};
}

#endif