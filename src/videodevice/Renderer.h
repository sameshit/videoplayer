//
//  GLRenderer.h
//  VideoPlayer
//
//  Created by Oleg on 04.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __VideoPlayer__Renderer__
#define __VideoPlayer__Renderer__

#include "renderers/BaseRenderer.h"

namespace VideoPlayerLib
{
    class Renderer
    {
    private:
        CoreObjectLib::CoreObject       *_core;
        CoreObjectLib::VideoHeader      _header;
        bool                            _opened;
        BaseRenderer                    *_renderer;
    public:
        Renderer(CoreObjectLib::CoreObject *core);
        virtual ~Renderer();
        
        // next methods should always be called from drawing(main) thread on any OS
        // otherwise you will see unpredictable behaviour (no drawing, crashing, etc.)
        bool    Open(const CoreObjectLib::VideoHeader &header,const DecoderLib::PixelFormat &pf
                     ,void *context_obj = NULL);
        bool    Close(void *context_obj = NULL);
        bool    Render(const CoreObjectLib::Frame &frame,void *context_obj = NULL);
        bool    Resize(const ImageSize &window_size,void *context_obj = NULL);
        bool    Clear(void *context_obj = NULL);
        void    FreeFrame(CoreObjectLib::Frame *frame) {_renderer->FreeFrame(frame);}
    };
}

#endif /* defined(__VideoPlayer__GLRenderer__) */
