#import <Cocoa/Cocoa.h>
#include "VideoLayer.h"

@interface FullScreenView : NSView
{
@private
    VideoLayer                          *_not_fs_layer;
    VideoLayer                          *_video_layer;
    CoreObjectLib::CoreObject           *_core;
    CoreObjectLib::VideoHeader               _header;
    DecoderLib::PixelFormat             _pf;
    bool                                _fullscreen;
}
- (id)initWithFrame:(NSRect)frame andCoreObject:(CoreObjectLib::CoreObject*)core_ andVideoHeader:(const CoreObjectLib::VideoHeader &)header_ andPixelFormat:(const DecoderLib::PixelFormat &)pf_ andNotFsLayer:(VideoLayer *)not_fs_layer_;
- (void)Draw:(const CoreObjectLib::Frame &)frame_;
- (bool)isFullScreen;
- (void)goFullScreen;
- (void) myRelease;
@end
