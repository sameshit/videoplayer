#include "../VideoDevice.h"

using namespace VideoPlayerLib;
using namespace CoreObjectLib;
using namespace DecoderLib;
using namespace PeerLib;

VideoDevice::VideoDevice(CoreObject *core)
	:_core(core),_fullscreen(false),_show_cursor(true),
_offscreen_window(nil),_video_layer(nil),_fs_view(nil),_started(false),_helper(nil)
{
}

VideoDevice::~VideoDevice()
{
    Close();
}


bool VideoDevice::Open(const VideoHeader &header,const PixelFormat &pf, CALayer *layer)
{
    if (_started)
    {
        COErr::Set("Already started");
        return false;
    }
    
    _layer = layer;
    _header = header;
    
    
    // NSWindow, NSView and CALayer doesn't init properly in not-main-thread(however apple docs say that it's possible). So there is a need for a helper class that will init rendering stuff in main thread
    _helper = [[MainThreadHelper alloc] initOffscreen:&_offscreen_window andVideoLayer:&_video_layer andFSView:&_fs_view andCALayer:layer andVideoHeader:header andPixelFormat:pf andCoreObject:_core];
    
    [_helper performSelectorOnMainThread:@selector(helpInit) withObject:nil waitUntilDone:YES];
    _started = true;
    
    return true;
}

bool VideoDevice::Close()
{
    if (!_started)
    {
        COErr::Set("Already stopped");
        return false;
    }
    
    [_helper performSelectorOnMainThread:@selector(helpFree) withObject:nil waitUntilDone:YES];
    
    [_helper release];
    _started = false;
    
    return true;
}

bool VideoDevice::ShowCursor()
{
// TO DO
    return true;
}

bool VideoDevice::HideCursor()
{
    return true;
}

bool VideoDevice::ToggleFullScreen()
{
    [_fs_view goFullScreen];
	return true;
}

void VideoDevice::Draw(const Frame &frame)
{
    if ([_fs_view isFullScreen])
        [_fs_view Draw:frame];
    else
        [_video_layer Draw:frame];
}