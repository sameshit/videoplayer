#import "VideoLayer.h"

using namespace VideoPlayerLib;
using namespace CoreObjectLib;
using namespace DecoderLib;
using namespace PeerLib;
using namespace std;

@implementation VideoLayer

- (id) initWithFrame:(CGRect)rect andCoreObject:(CoreObject*)core_ andVideoHeader:(const VideoHeader&)header_ andPixelFormat:(const PixelFormat &)pf_
{
    self = [super init];
    if (self)
    {
        self->_core = core_;
        self->_header = header_;
        self->_pf = pf_;
        
        
        [self setAsynchronous:NO];
        [self setAutoresizingMask:(kCALayerWidthSizable | kCALayerHeightSizable)];
        [self setFrame:rect];
    }
    return self;
}

- (void)myRelease
{
    _lock.Lock();
    if (_frame.data != NULL)
    {
        _renderer->FreeFrame(&_frame);
        _frame.data = NULL;
    }
    
    fast_delete(_renderer);
    _renderer = NULL;
    _lock.UnLock();
}

- (BOOL)canDrawInCGLContext:(CGLContextObj)glContext pixelFormat:(CGLPixelFormatObj)pixelFormat forLayerTime:(CFTimeInterval)timeInterval displayTime:(const CVTimeStamp *)timeStamp
{    
	return YES;
}

- (CGLContextObj)copyCGLContextForPixelFormat:(CGLPixelFormatObj)pixelFormat
{
	CGLContextObj context;
    ImageSize window_size,frame_size;
    NSRect rect;

    context = [super copyCGLContextForPixelFormat:pixelFormat];
    _frame.data = NULL;
    fast_new(_renderer, _core);
    
    if (context)
    {
        CGLSetCurrentContext(context);
    
        ASSERT_ERROR(_renderer->Open(_header,_pf,context));

        rect = NSRectFromCGRect([self bounds]);
    
        window_size.width = rect.size.width;
        window_size.height = rect.size.height;
    
        frame_size.width = _header.width;
        frame_size.height = _header.height;
        
        ASSERT_ERROR(_renderer->Resize(window_size,context));
    }
    
	return context;
}

-(void)releaseCGLContext:(CGLContextObj)glContext
{
    [super releaseCGLContext:glContext];
}

- (void)drawInCGLContext:(CGLContextObj)ctx pixelFormat:(CGLPixelFormatObj)pf forLayerTime:(CFTimeInterval)t displayTime:(const CVTimeStamp *)ts 
{
    Frame frame;
    bool has_frame;
    bool has_renderer;
     
    _lock.Lock();
    has_frame = _frame.data != NULL;
    frame = _frame;
    has_renderer = _renderer != NULL;
    _lock.UnLock();
    
    if (has_renderer)
    {
        if (has_frame)
            _renderer->Render(frame,ctx);
        else
            _renderer->Clear(ctx);
    }
    
    [super drawInCGLContext:ctx pixelFormat:pf forLayerTime:t displayTime:ts];
}

- (void)Draw:(const Frame &)frame_
{
    _lock.Lock();
    if (_frame.data != NULL)
        _renderer->FreeFrame(&_frame);
    _frame = frame_;
    _lock.UnLock();
    
    [self performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:nil waitUntilDone:NO];
    
}
@end
