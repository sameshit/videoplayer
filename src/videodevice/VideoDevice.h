#ifndef VIDEODEVICE_H_
#define VIDEODEVICE_H_

#include "../../Decoder/src/Decoder.h"
#include "Renderer.h"

#ifdef OS_X
#import "osx/FullScreenView.h"
#import "osx/MainThreadHelper.h"
#import <OpenGL/OpenGL.h>
#endif

#ifdef OS_LINUX
#include "linux/FullScreenWidget.h"
#endif

namespace VideoPlayerLib
{
class LIBEXPORT VideoDevice
{
public:
    VideoDevice(CoreObjectLib::CoreObject *core);
	virtual ~VideoDevice();
    
#if defined(OS_X)
	bool Open(const CoreObjectLib::VideoHeader &header,const DecoderLib::PixelFormat &pf,CALayer *layer);
#elif defined(OS_WIN)
	bool Open(const CoreObjectLib::VideoHeader &header,const DecoderLib::PixelFormat &pf, HWND hwnd);
#endif

    bool Close();
    bool Draw(CoreObjectLib::Frame *frame);

	bool ToggleFullScreen();
	bool HideCursor();
	bool ShowCursor();
private:
	CoreObjectLib::CoreObject *_core;
    CoreObjectLib::VideoHeader _header;
	bool _show_cursor;
	bool _is_opened;
	bool _fullscreen;
#ifdef OS_LINUX
    GtkWidget *_widget;
    FullScreenWidget *_fs_widget;
    bool _gl_init;
    static uint32_t _ref_count;
    static bool _glew_init;
    static GdkCursor *_cur,*_no_cur;
    static gboolean ResizeEvent(GtkWidget *da, GdkEventExpose *event, gpointer user_data);
    static gboolean ScheduleDrawEvent(gpointer user_data);
    static gboolean DrawEvent(GtkWidget *da, GdkEventExpose *event, gpointer user_data);
#elif defined(OS_WIN)
	HWND			_hwnd,_parent;
	HDC				_hdc;
	HGLRC			_hrc;
	DWORD			_window_width,_window_height;
	Renderer		*_renderer;
	CoreObjectLib::SpinLock _draw_lock;
	static LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
   	bool CreateChildWindow();
#elif defined(OS_X)
    CALayer             *_layer;
    VideoLayer          *_video_layer;
    FullScreenView      *_fs_view;
    NSWindow            *_offscreen_window;
    MainThreadHelper    *_helper;
#endif
};

}
#endif /* VIDEODEVICE_H_ */
