//
//  GLRenderer.cpp
//  VideoPlayer
//
//  Created by Oleg on 04.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "Renderer.h"
#include "renderers/YUV420Renderer.h"
#ifdef OS_X
#include "renderers/osx/VDARenderer.h"
#endif

using namespace VideoPlayerLib;
using namespace CoreObjectLib;
using namespace PeerLib;
using namespace DecoderLib;

Renderer::Renderer(CoreObject *core)
:_core(core),_renderer(NULL),_opened(false)
{

}

Renderer::~Renderer()
{
    Close();
}

bool Renderer::Open(const VideoHeader &header,const PixelFormat &pf, void *context_obj)
{
    if (_opened)
    {
        COErr::Set("Renderer is already in opened state");
        return false;
    }
    
    switch (pf)
    {
#ifdef OS_X
        case PF_VDA:
            typed_new(VDARenderer,_renderer,_core);
        break;
#endif
        case PF_YUV420p:
            typed_new(YUV420Renderer,_renderer,_core);
        break;
        default:
            COErr::Set("Unknown pixel format in Renderer::Open()",pf);
            return false;
        break;
    }
    
    _opened = _renderer->Open(header);
    if (!_opened)
    {
        fast_delete(_renderer);
        _renderer = NULL;
    }
    return _opened;
}

bool Renderer::Close(void *context_obj)
{
    bool ret_val;
    if (!_opened)
    {
        COErr::Set("Renderer is not in opened state in Close()");
        return false;
    }
    
    ret_val = _renderer->Close(context_obj);
    fast_delete(_renderer);
    return ret_val;
}

bool Renderer::Resize(const ImageSize &window_size,void *context_obj)
{
    if (!_opened)
    {
        COErr::Set("Renderer is not in opened state in Resize()");
        return false;
    }
    
    return _renderer->Resize(window_size,context_obj);
}

bool Renderer::Render(const Frame &frame,void *context_obj)
{
    if (!_opened)
    {
        COErr::Set("Renderer is not in opened state in Render()");
        return false;
    }
    
    return _renderer->Render(frame,context_obj);
}

bool Renderer::Clear(void *context_obj)
{
    if (!_opened)
    {
        COErr::Set("Renderer is not in opened state in Clear()");
        return false;
    }
    
    return _renderer->Clear(context_obj);
}